import React from 'react';

const Loader = () => {
  return (
    <div className='movies__loader flex items-center justify-center'>
      <div className='loader'></div>
  </div>
  )
}

export { Loader };