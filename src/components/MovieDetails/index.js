import React, {useCallback, useState} from 'react';
import PropTypes from 'prop-types';
import movieDetails from './movieDetails.css';
import postStreamings from '../../services/api/endpoints/post_streamings';
import {xhrHandler} from '../../services/api/connector';
import { Hero } from '../Hero';

const MovieDetails = ({data}) => {
  const snapshotURL = data.images.snapshot;
  const styles = {background: `url(${snapshotURL}) no-repeat`}
  const [trailerUrl, setTrailerUrl] = useState(null);

  const loadTrailer = useCallback(() => {
    const postStreamingsParams = {...postStreamings.params, content_id: data.id};
    xhrHandler({...postStreamings, params: postStreamingsParams})
      .then(payload => {
        const url = payload.data.data.stream_infos[0].url;
        setTrailerUrl(url);
      })
  }, []);

  return (
    <Hero img={snapshotURL} fullScreen={true}>
      {!trailerUrl &&
        <button className='movie__details-play-button flex items-center justify-center' 
                onClick={loadTrailer}>
          ▶
        </button>
      }
      {trailerUrl && 
        <video className='movie__details-player' autoPlay controls>
          <source src={trailerUrl} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      }

    </Hero>
  )
};


MovieDetails.propTypes = {
  data: PropTypes.object
}

export { MovieDetails }