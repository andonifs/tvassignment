import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Carousel} from '../../components/Carousel';
import {Loader} from '../Loader';
import moviesStyles from './movieslistStyles.css';

const renderMovies = (item) => {
  return (
    <React.Fragment key={item.id}>
      <Link className='movies__headline text-white text-no-underline' 
            to='/' >
        {item.name}
      </Link>
      <Carousel items={item.contents.data} />
    </React.Fragment>
  );
};

const MoviesList = ({items, loading}) => {

  let content = <Loader />;
  
  if(!loading) {
    content =  items.map(renderMovies);
  }

  return (
    <section className='movies flex flex-column'>
      {content}
    </section>
  )
};

MoviesList.propTypes = {
  items: PropTypes.array,
  loading: PropTypes.bool
}

export {MoviesList}