import React, { useCallback, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import placeholder from '../../../assets/img/placeholder.png';
import {CarouselArrow} from './CarouselArrow';
import carouselStyles from './carouselStyles.css';
import {lazyLoad} from '../../services/images/lazyLoad';
import {debounce} from '../../services/events/debounce';

const renderItems = (item, index) => {
  return <Link to={`/movies/${item.id}`} className='carousel__item relative' key={item.id} >
    <img className='carousel__artwork lazy'
              src={placeholder} 
              data-src={item.images.artwork}
              alt={item.title} />
  </Link>;
}

const getVisibleItems = () => {
  if(window.innerWidth > 1140) {
    return 6;
  } else if(window.innerWidth > 910) {
    return 5;
  } else if(window.innerWidth > 730) {
    return 4;
  } else if(window.innerWidth > 560) {
    return 3;
  } 
  return 2;
};

const getMaxStep = (totalItems) => {
  const visibleItems = getVisibleItems();
  const totalSteps = Math.floor(totalItems/visibleItems);
  if(totalItems % visibleItems) {
    return totalSteps;
  }
  return totalSteps - 1;
}

const Carousel = ({items}) => {

  const [step, setStep] = useState(0);
  const carouselRef = useRef(null);
  
  useEffect(() => { 
    onMount();
    () => {
      onUnmount();
    }
  }, []);

  useEffect(() => { scrollCarousel() }, [step]);

  const onMount = () => {
    lazyLoad({target: carouselRef.current}); 
    window.addEventListener('scroll', debounce(lazyLoad, 200));
    carouselRef.current.addEventListener('transitionend', lazyLoad);
  };

  const onUnmount = () => {
    window.removeEventListener('scroll', lazyLoad);
    carouselRef.current.removeEventListener('transitionend', lazyLoad);
  };

  const scrollCarousel = () => {
    if(carouselRef.current) {
      const visibleItems = getVisibleItems();
      const scrollValue = (100/(visibleItems + 0.5))*visibleItems;
      carouselRef.current.style.transform = `translateX(${scrollValue * step}%)`;
    }
  };

  const nextStep = useCallback(() => {
    setStep(step - 1);
  }, [step]);
  
  const prevStep = useCallback(() => {
      setStep(step + 1);
  }, [step]);

  return ( 
    <div className='carousel relative overflow-hidden'>
        {step !== 0 &&
          <CarouselArrow direction='left' action={prevStep} />
        }
        <div className='carousel__items' data-testid="carouselItems" ref={carouselRef}>
            {items.map(renderItems)}
        </div>
        {step > -getMaxStep(items.length) && 
          <CarouselArrow direction='right' action={nextStep} testId='nextStep' />
        }
    </div>
  );
};

Carousel.propTypes = {
  items: PropTypes.array
};

export {Carousel, getVisibleItems, getMaxStep, renderItems };