import React, {useCallback} from 'react';
import PropTypes from 'prop-types';

const CarouselArrow = ({action, direction, testId}) => {

  const handleClick = useCallback(() => {
    action();
  }, [action]);

  return (
    <div className={`carousel__arrow 
    carousel__arrow--${direction} 
    absolute flex 
    items-center 
    justify-center 
    h-full`}
    data-testid={testId}
    onClick={handleClick}></div>
  )
}

CarouselArrow.propTypes = {
  action: PropTypes.func,
  direction: PropTypes.string,
  testId: PropTypes.string
};

export { CarouselArrow };