import React from 'react';
import PropTypes from 'prop-types';
import heroStyles from './heroStyles.css';

const Hero = ({img, fullScreen, children}) => {
  let styles = {};
  let classes = 'hero w-full flex items-center justify-center';

  if(img) {
    styles = {background: `url(${img}) no-repeat`, backgroundSize: 'cover'};
  }

  if(fullScreen) {
    classes += ' full--screen';
  }

  return (
    <section className={classes} 
             style={styles}>
      {children}
    </section>
  );
};

Hero.propTypes = {
  children: PropTypes.array,
  fullScreen: PropTypes.bool,
  img: PropTypes.string
};

export { Hero };