import * as React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Carousel} from '../components/Carousel';
import {CarouselArrow} from '../components/Carousel/CarouselArrow';
import { act } from "react-dom/test-utils";
import { fireEvent, render, getByTestId } from "@testing-library/react";
import { BrowserRouter as Router } from 'react-router-dom';
import * as carousel from '../components/Carousel';

Enzyme.configure({ adapter: new Adapter() });

const items = [
  {
    id: 'test', 
    images: { 
      artwork: 'test.jpg' 
    },
    title: 'test' 
  },
  {
    id: 'test-1', 
    images: { 
      artwork: 'test.jpg' 
    },
    title: 'test' 
  }, 
  {
    id: 'test-2', 
    images: { 
      artwork: 'test.jpg' 
    },
    title: 'test' 
  },  
  {
    id: 'test-3', 
    images: { 
      artwork: 'test.jpg' 
    },
    title: 'test' 
  },
  {
    id: 'test-4', 
    images: { 
      artwork: 'test.jpg' 
    },
    title: 'test' 
  }
];

// const wrapper = mount(<Carousel items={items} />);

afterEach(() => {
  jest.clearAllMocks();
});

test('get visible items according to viewport size', () => {
 
  global.innerWidth = 1200;
  let visibleItems = carousel.getVisibleItems();
  expect(visibleItems).toBe(6);

  global.innerWidth = 915;
  visibleItems = carousel.getVisibleItems();
  expect(visibleItems).toBe(5);

  global.innerWidth = 750;
  visibleItems = carousel.getVisibleItems();
  expect(visibleItems).toBe(4);

  global.innerWidth = 565;
  visibleItems = carousel.getVisibleItems();
  expect(visibleItems).toBe(3);

  global.innerWidth = 1;
  visibleItems = carousel.getVisibleItems();
  expect(visibleItems).toBe(2);

  global.innerWidth = 1024;
});

test('get max step according to total items and visible items', () => {
  let maxStep = carousel.getMaxStep(18);
  expect(maxStep).toBe(3);
  maxStep = carousel.getMaxStep(15);
  expect(maxStep).toBe(2);
});

test('renders carousel items is an <img /> with specific attributes', () => {
  
  const config = {
    id: 'test',
    images: {
      artwork: 'test.jpg'
    },
    title: 'Test'
  };

  const item = carousel.renderItems(config);
  const expectedProps = {
    to: '/movies/test',
    className: 'carousel__item relative',
    children: <img alt="Test" className="carousel__artwork lazy" data-src="test.jpg" src="placeholder.png" />
  };
  expect(item.props).toEqual(expectedProps);
  expect(item.type.displayName).toBe('Link');
});

test('carousel translates', () => {
  global.innerWidth = 200;

  const {container} = render(<Router><Carousel items={items} /></Router>);
  const carouelItems = getByTestId(container, "carouselItems");
  const nextStep = getByTestId(container, "nextStep");

  expect(carouelItems.style.transform).toBe('translateX(0%)');

  fireEvent.click(nextStep)

  expect(carouelItems.style.transform).toBe('translateX(-80%)');
});

test('step state decreases when click on right arrow', () => {
  const setState = jest.fn();
  const state = 0;
  const useStateMock = (initState) => [initState = state, setState];
  jest.spyOn(React, 'useState').mockImplementation(useStateMock); 

  global.innerWidth = 200;

  const wrapper = mount(<Router><Carousel items={items} /></Router>);
  const arrowRight = wrapper.find(CarouselArrow).children();

  act(()=> arrowRight.props().onClick());
  expect(setState).toHaveBeenCalledWith(state - 1);
});

test('step state increases when click on left arrow', () => {
  const setState = jest.fn();
  const state = -1;
  const useStateMock = (initState) => [initState = state, setState];
  jest.spyOn(React, 'useState').mockImplementation(useStateMock); 

  const wrapper = mount(<Router><Carousel items={items} /></Router>);
  const arrowLeft = wrapper.find(CarouselArrow).first().children();

  act(()=> arrowLeft.props().onClick());
  expect(setState).toHaveBeenCalledWith(state + 1 );
});

test('step equals to maxStep only renders left arrow', () => {
  const setState = jest.fn();
  const state = -2;
  const useStateMock = (initState) => [initState = state, setState];
  jest.spyOn(React, 'useState').mockImplementation(useStateMock); 

  global.innerWidth = 200;

  const wrapper = mount(<Router><Carousel items={items} /></Router>);
  const carouselArrow = wrapper.find(CarouselArrow);
  expect(carouselArrow).toHaveLength(1);
  expect(carouselArrow.props().direction).toBe('left');
});

test('step in between 0 and maxStep only renders both arrows arrow', () => {
  const setState = jest.fn();
  const state = -1;
  const useStateMock = (initState) => [initState = state, setState];
  jest.spyOn(React, 'useState').mockImplementation(useStateMock); 

  global.innerWidth = 200;

  const wrapper = mount(<Router><Carousel items={items} /></Router>);

  const carouselArrow = wrapper.find(CarouselArrow);
  expect(carouselArrow).toHaveLength(2);
  expect(carouselArrow.first().props().direction).toBe('left');
  expect(carouselArrow.last().props().direction).toBe('right');
});

test('step 0 only renders right arrow', () => {
  const setState = jest.fn();
  const state = 0;
  const useStateMock = (initState) => [initState = state, setState];
  jest.spyOn(React, 'useState').mockImplementation(useStateMock); 

  global.innerWidth = 200;

  const wrapper = mount(<Router><Carousel items={items} /></Router>);
  const carouselArrow = wrapper.find(CarouselArrow);
  expect(carouselArrow).toHaveLength(1);
  expect(carouselArrow.props().direction).toBe('right');
});
