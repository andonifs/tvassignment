import {xhrHandler, getAllApiCalls} from '../services/api/connector';

test('throw error if no verb matches', () => {
  const config = {verb: null, paths: ['/v1/test']};
  expect(() => connector.xhrHandler(config)).toThrow();
});

// test('', async () => {
//   const params = {apiURL: 'host.com', paths: ['/v1/test', '/v1/a'], params:{}};
//   const apiCalls = getAllApiCalls(params);
//   expect(apiCalls).toStrictEqual([expect.any(Promise), expect.any(Promise)]);
//   expect(apiCalls).toEqual(expect.any(Array));
//   expect(apiCalls).toHaveLength(2);
// });