import { lazyLoad } from '../services/images/lazyLoad';
import placeholder from '../../assets/img/placeholder.png';

document.body.innerHTML = `<div id="test">
    <img class='lazy' data-src='img.jpg' height="600" width="300" src='placeholder.jpg' />
</div>`;

test('Loads img src when imgs are within vertical viewport height', () => {
  const target = document.querySelector('#test');
  const img = document.querySelector('.lazy');
  
  expect(img.src).toBe('http://localhost/placeholder.jpg');
  lazyLoad({target});
  expect(img.src).toBe('http://localhost/img.jpg');
});