function debounce(callback, delay) {
  let timer = null;

  return function() {
    if (timer) return;

    callback.apply(this, arguments);
    timer = setTimeout(() => timer = null, delay);
  }
}

export { debounce };