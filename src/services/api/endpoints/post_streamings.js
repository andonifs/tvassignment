export default {
  path: '/v3/me/streamings',
  verb: 'POST',
  params: {
    audio_language:'SPA', 
    audio_quality:'2.0', 
    content_type:'movies', 
    device_serial: 'device_serial_1', 
    device_stream_video_quality:'FHD', 
    player:'web:PD-NONE', 
    subtitle_language:'MIS', 
    video_type:'trailer'
  },
  urlParams: {
    classification_id: 5,
    device_identifier: 'web',
    locale: 'es',
    market_code: 'es'
  }
};
