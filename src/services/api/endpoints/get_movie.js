const getMovie = (id) => {
  return {
    paths: [`/v3/movies/${id}`],
    verb: 'GET',
    params: {
        classification_id: 5,
        device_identifier: 'web',
        locale: 'es',
        market_code: 'es'
    }
  }
};

export { getMovie };
