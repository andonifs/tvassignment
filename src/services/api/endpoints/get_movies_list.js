export default {
  paths: [
    '/v3/lists/populares-en-taquilla', 
    '/v3/lists/estrenos-para-toda-la-familia',
    '/v3/lists/estrenos-imprescindibles-en-taquilla',
    '/v3/lists/estrenos-espanoles',
    '/v3/lists/si-te-perdiste',
    '/v3/lists/nuestras-preferidas-de-la-semana'
  ],
  verb: 'GET',
  params: {
      classification_id: 5,
      device_identifier: 'web',
      locale: 'es',
      market_code: 'es'
  }
};
