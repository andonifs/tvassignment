import axios from 'axios';
import {dispatch} from '../../containers/Provider';
import { API_URL } from '../../config';
import { SET_ERROR } from '../../containers/Provider/constants';

//We are using axios, but we could use any other library and change the xhr connector
const xhrHandler = (config) => {
  switch(config.verb) {
    case 'POST':
      return postData(config);
    case 'GET':
      return getData(config);
      break;
    default:
      throw new Error('A verb must be provided to the xhrHandler');
  }
};

const postData = ({apiURL = API_URL, path, urlParams, params}) => {
  return axios.post(`${apiURL}${path}`, params, {params: urlParams})
  .catch(e => console.error(e)); //Todo Handling error
};

const getData = (config) => {
  return axios.all(getAllApiCalls(config))
  .catch(e => console.error(e)); //Todo Handling error
};

const getAllApiCalls = ({apiURL = API_URL, paths, params}) => {
  return paths.map(path => {
    return axios.get(`${apiURL}${path}`, {params});
  });
}

export { xhrHandler, getAllApiCalls };