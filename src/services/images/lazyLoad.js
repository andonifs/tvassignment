const lazyLoad = ({target}) => {
  const images = target.querySelectorAll('.lazy');
  images.forEach(img => {
    const viewportOffset = img.getBoundingClientRect();
    const src = img.dataset.src;
    const itemsHorizontallyVisible = viewportOffset.left <= window.innerWidth * 2;
    const currentHeightView = window.scrollY + window.innerHeight;
    const itemsVerticallyVisible = viewportOffset.top <= currentHeightView;
    if(itemsHorizontallyVisible && itemsVerticallyVisible && img.src !== src) {
        img.src = src;
    }
  })
};

export { lazyLoad };