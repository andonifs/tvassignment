import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {xhrHandler} from '../../services/api/connector';
import {useParams} from 'react-router-dom';
import {getMovie} from '../../services/api/endpoints/get_movie';
import { Loader } from '../../components/Loader';
import {GET_MOVIE_DETAILS} from '../Provider/constants';
import {MovieDetails} from '../../components/MovieDetails';

const Movie = ({dispatch, movieDetails}) => {
  
  useEffect(() => onMount(), []);

  const [loading, setLoading] = useState(true);
  const { movieId } = useParams();

  const onMount = () => {
    const action = getMovie(movieId);
    xhrHandler(action).then(payload => {
        const [data] = getMovieDetails(payload)
        dispatch({type: GET_MOVIE_DETAILS, data})
        setLoading(false);
      })
      .catch(e => console.log('error'));
  };

  const getMovieDetails = (items) => {
    return items.map(item => {
      return item.data.data;
    })
  }

  let content = <Loader />;
  
  if(!loading) {
    content = <MovieDetails data={movieDetails} movieId={movieId} />
  }

  return (
    <section className='h-full'>
      { content }
    </section>
  );
}

Movie.propTypes = {
  dispatch: PropTypes.func,
  movieDetails: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ])
};

export { Movie };