import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import getLists from '../../services/api/endpoints/get_movies_list';
import {xhrHandler} from '../../services/api/connector';
import {GET_MOVIES_LISTS} from '../Provider/constants';
import {MoviesList} from '../../components/MovieList';
import {Hero} from '../../components/Hero';

const Home = ({featuredMovies, dispatch}) => {

  useEffect(() => onMount(), []);
  
  const [loading, setLoading] = useState(true);

  const onMount = () => {
    xhrHandler(getLists).then(payload => {
        const data = getFeaturedMoviesList(payload)
        dispatch({type: GET_MOVIES_LISTS, data})
        setLoading(false);
      })
  };

  const getFeaturedMoviesList = (items) => {
    return items.map(item => {
      return item.data.data;
    })
  }

  return (
    <>
      <Hero />
      <MoviesList items={featuredMovies} loading={loading} />
    </>
  )
}

Home.propTypes = {
  dispatch: PropTypes.func,
  featuredMovies: PropTypes.array
};

export { Home };