import { 
  GET_MOVIE_DETAILS, 
  GET_MOVIES_LISTS,
  SET_ERROR 
} from "./constants";

const initialState = {
  error: null,
  featuredMovies: [],
  movieDetails: [],
};

const providerReducer = (state, action) => {
  switch(action.type) {
    case   GET_MOVIE_DETAILS:
      return {...state, movieDetails: action.data} 
    case GET_MOVIES_LISTS:
      return {...state, featuredMovies: action.data} 
    case SET_ERROR:
        return {...state, error: action.error} 
    default:
        throw new Error();
  }
};

export { initialState, providerReducer };