import React, {useCallback, useReducer} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import {Menu} from '../../components/Menu';
import {Home} from '../Home';
import {Movie} from '../Movie';

import {providerReducer, initialState} from './providerReducer';

const Provider = () => {

  const [store, dispatch] = useReducer(providerReducer, initialState);

  return (
    <Router>
      <section className='container'>
        <Menu movieTitle={store.movieDetails.original_title} id={store.movieDetails.id} />
        <Switch>
          <Route path="/movies/:movieId">
            <Movie  dispatch={dispatch} movieDetails={store.movieDetails} />
          </Route> 
          <Route exact path="/">
            <Home dispatch={dispatch} featuredMovies={store.featuredMovies} />
          </Route>
        </Switch>
        </section>
    </Router>
  );
};

export { Provider };