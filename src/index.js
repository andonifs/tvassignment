import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from './containers/Provider';
import global from './styles/global.css';

const app = document.querySelector('#root');

ReactDOM.render(<Provider />, app);

