module.exports = {
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/src/__mocks__/assetsTransformer.js",
    "\\.(css)$": "<rootDir>/src/__mocks__/assetsTransformer.js"
  },
  projects: ['./src'],
  setupFilesAfterEnv: ['<rootDir>/node_modules/jest-enzyme/lib/index.js']
};
