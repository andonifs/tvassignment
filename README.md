# TV Test

- Movies list and details 
- Show trailer for movies

# Running

Test requires [Node.js](https://nodejs.org/) to run.

By default the App server runs in port _8080_.

First `npm install` to grab all the necessary dependencies.

**App dependencies**

```sh
$ cd client
$ npm install
```

**Run App**

```sh
$ cd client
$ npm run dev
```

**App Build**

```sh
$ cd client
$ npm run build
```

**App Tests**

```sh
$ cd client
$ npm run test
```

# Contact Email

andoni282@gmail.com
